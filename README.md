# Docker Image for XRDP, forked from [FEROX](https://github.com/frxyt/docker-xrdp)

![Docker Pulls](https://img.shields.io/docker/pulls/michalklabisz/xrdp.svg)
![GitHub last commit](https://img.shields.io/gitlab/last-commit/mk_cafe/docker-xrdp.svg)

This image packages XRDP and VNC.

* Docker Hub: https://hub.docker.com/r/michalklabisz/xrdp
* GitHub: https://gitlab.com/mk_cafe/docker-xrdp

## Docker Hub Image

**`michalklabisz/xrdp`**

### Supported tags

* **`michalklabisz/xrdp:cinnamon`**: with [Cinnamon](http://developer.linuxmint.com/projects/cinnamon-projects.html)
* **`michalklabisz/xrdp:latest`**: *without any desktop, only XRDP with VNC*
* **`michalklabisz/xrdp:lxde`**: with [LXDE](https://lxde.org/)
* **`michalklabisz/xrdp:mate`**: with [MATE](https://mate-desktop.org/)
* **`michalklabisz/xrdp:xfce`**: with [Xfce](https://www.xfce.org/)
* **`michalklabisz/xrdp:enlightenment`**: with [Enlightenment](https://www.enlightenment.org/)
* **`michalklabisz/xrdp:icewm`**: with [IceWM](https://ice-wm.org/)
* **`michalklabisz/xrdp:wmaker`**: with [Window Maker](https://www.windowmaker.org/)

## Usage

### Try it

1. Run an image with a pre-installed desktop:
   * Cinnamon: `docker run --rm -p 33890:3389 michalklabisz/xrdp:cinnamon`
   * LXDE: `docker run --rm -p 33890:3389 michalklabisz/xrdp:lxde`
   * MATE: `docker run --rm -p 33890:3389 michalklabisz/xrdp:mate`
   * Xfce: `docker run --rm -p 33890:3389 michalklabisz/xrdp:xfce`
   * Enlightenment: `docker run --rm -p 33890:3389 michalklabisz/xrdp:enlightenment`
   * IceWM: `docker run --rm -p 33890:3389 michalklabisz/xrdp:icewm`
   * Window Maker: `docker run --rm -p 33890:3389 michalklabisz/xrdp:wmaker`
1. Start a RDP client:
   * Windows: press `Win+R`, run `mstsc`, connect to: `localhost:33890`
1. Enter default credentials: user `debian`, password `ChangeMe`
1. Enjoy !

### Configurable environment variables

These environment variables can be overriden to change the default behavior of the image and adapt it to your needs:

| Name                     | Default value                                       | Example                                          | Description
| :------------------------| :-------------------------------------------------- | :----------------------------------------------- | :----------
| `MK_APTGET_DISTUPGRADE` | ` ` *(Empty)*                                       | `1`                                              | Update installed packages
| `MK_APTGET_INSTALL`     | ` ` *(Empty)*                                       | `midori terminator`                              | Packages to install with `apt-get`
| `MK_CMD_INIT`           | ` ` *(Empty)*                                       | `echo 'Hello World !'`                           | Command to run before anything else
| `MK_CMD_START`          | ` ` *(Empty)*                                       | `echo 'Hello World !'`                           | Command to run before starting services
| `MK_LOG_PREFIX_MAXLEN`  | `6`                                                 | `10`                                             | Maximum length of prefix displayed in logs
| `MK_XRDP_CERT_SUBJ`     | `/C=FX/ST=None/L=None/O=None/OU=None/CN=localhost`  | `/C=FR/ST=67/L=SXB/O=MK/OU=IT/CN=xrdp.mk.yt` | XRDP certificate subject
| `MK_XRDP_USER_NAME`     | `debian`                                            | `john.doe`                                       | Default user name
| `MK_XRDP_USER_PASSWORD` | `ChangeMe`                                          | `myNOTsecretPassword`                            | Default user password
| `MK_XRDP_USER_SUDO`     | `1`                                                 | `0`                                              | Add default user to `sudoers` if set to `1`
| `MK_XRDP_USER_GID`      | `1000`                                              | `33`                                             | Default user ID (UID)
| `MK_XRDP_USER_UID`      | `1000`                                              | `33`                                             | Default user group ID (GID)
| `MK_XRDP_USER_COPY_SA`  | `0`                                                 | `1`                                              | Copy default icons to desktop if set to `1`
| `TZ`                     | `Etc/UTC`                                           | `Europe/Paris`                                   | Default time zone

### Example

#### Basic example

To run this image, you can use this sample `docker-compose.yml` file:

```yaml
php:
  image: michalklabisz/xrdp:xfce
  environment:
    - MK_XRDP_USER_NAME=john.doe
    - MK_XRDP_USER_PASSWORD=MyPassword
  ports:
    - "22000:22"
    - "3389:3389"
  volumes:
    - ./home:/home:rw
```

#### Full PHP development environment with Apache, MySQL, DBeaver and VS Code

1. Create this `docker-compose.yml` file:
   ```yaml
   version: '3.7'

   services: 
     xrdp:
       image: michalklabisz/xrdp:xfce
       environment:
         - |
           MK_CMD_INIT=curl -sSL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /usr/share/keyrings/packages.microsoft.gpg
           echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/   vscode.list
           curl -sSL https://dbeaver.io/debs/dbeaver.gpg.key | apt-key add -
           echo "deb https://dbeaver.io/debs/dbeaver-ce /" > /etc/apt/sources.list.d/dbeaver.list
         - |
           MK_APTGET_INSTALL=apache2 libapache2-mod-php
           code
           dbeaver-ce
           default-mysql-server php-mysql php-pdo
           firefox-esr
           php php-bcmath php-cli php-common php-curl php-gd php-json php-mbstring php-pear php-xdebug php-xml php-zip
         - |
           MK_CMD_START=
           rm -f /var/run/apache2/apache2.pid
           echo "UPDATE mysql.user SET Password=PASSWORD('root') WHERE User='root'; FLUSH PRIVILEGES;" > /etc/mysql/init.sql
           echo -e "[program:apache2]\ncommand=/usr/sbin/apache2ctl -DFOREGROUND" > /etc/supervisor/conf.d/apache2.conf
           echo -e "[program:mysqld]\ncommand=/usr/bin/mysqld_safe --init-file=/etc/mysql/init.sql" > /etc/supervisor/conf.d/mysqld.conf
       ports:
         - "22000:22"
         - "33890:3389"
   ```
1. Run `docker-compose up`

### Execute custom scripts upon startup

You can copy your executable scripts in `/mk/entrypoint.d/` and they'll be executed in alphabetical order right before `supervisor` is started.

### Start custom process in background

You can use `supervisor` to start them and place all the services you need as `.conf` files in `/etc/supervisor/conf.d/`.

## Build

```sh
docker build -f Dockerfile -t xrdp:latest .
```

## License

This project and images are published under the [MIT License](LICENSE).

```
MIT License

Copyright (c) 2019 FEROX YT EIRL, www.ferox.yt <devops@ferox.yt>
Copyright (c) 2019 Jérémy WALTHER <jeremy.walther@golflima.net>
Copyright (c) 2023 Michał Klabisz <michal.klabisz@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```