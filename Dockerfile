# Copyright (c) 2019 FEROX YT EIRL, www.ferox.yt <devops@ferox.yt>
# Copyright (c) 2019 Jérémy WALTHER <jeremy.walther@golflima.net>
# Copyright (c) 2023 Michał Klabisz <michal.klabisz@gmail.com>
# See <https://gitlab.com/mk_cafe/docker-xrdp> for details.

FROM debian:buster

LABEL maintainer="Michal Klabisz <michal.klabisz@gmail.com>"

# Install required packages to run
RUN     DEBIAN_FRONTEND=noninteractive apt-get update \
    &&  DEBIAN_FRONTEND=noninteractive apt-get upgrade -y --fix-missing --no-install-recommends \
            ca-certificates \
            curl \
            dbus-x11 \
            gnupg \
            openssh-server \
            sudo \
            supervisor \
            tigervnc-standalone-server \
            vim \
            xrdp \
            xterm \
            firefox-esr \
            mc \
            unzip \
    &&  apt-get clean -y && apt-get clean -y && apt-get autoclean -y && rm -r /var/lib/apt/lists/*

# Set default environment variables
ENV MK_APTGET_DISTUPGRADE= \
    MK_APTGET_INSTALL= \
    MK_CMD_INIT= \
    MK_CMD_START= \
    MK_LOG_PREFIX_MAXLEN=6 \
    MK_XRDP_CERT_SUBJ='/C=FX/ST=None/L=None/O=None/OU=None/CN=localhost' \
    MK_XRDP_USER_NAME=debian \
    MK_XRDP_USER_PASSWORD=ChangeMe \
    MK_XRDP_USER_SUDO=1 \
    MK_XRDP_USER_GID=1000 \
    MK_XRDP_USER_UID=1000 \
    MK_XRDP_USER_COPY_SA=0 \
    TZ=Etc/UTC

# Copy assets
COPY build/log                  /usr/local/bin/mk-log
COPY build/start                /usr/local/sbin/mk-start
COPY build/supervisord.conf     /etc/supervisor/supervisord.conf
COPY build/xrdp.ini             /etc/xrdp/xrdp.ini

# Configure installed packages
RUN     echo "ALL ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/ALL \
    &&  sed -e 's/^#\?\(PermitRootLogin\)\s*.*$/\1 no/' \
            -e 's/^#\?\(PasswordAuthentication\)\s*.*$/\1 yes/' \
            -e 's/^#\?\(PermitEmptyPasswords\)\s*.*$/\1 no/' \
            -e 's/^#\?\(PubkeyAuthentication\)\s*.*$/\1 yes/' \
            -i /etc/ssh/sshd_config \
    &&  mkdir -p /run/sshd \
    &&  mkdir -p /var/run/dbus \
    &&  mkdir -p /mk/entrypoint.d \
    &&  rm -f /etc/xrdp/cert.pem /etc/xrdp/key.pem /etc/xrdp/rsakeys.ini \
    &&  rm -f /etc/ssh/ssh_host_*

# Prepare default desktop if needed & version information
ARG DOCKER_TAG
ARG SOURCE_BRANCH
ARG SOURCE_COMMIT
COPY build/desktop /usr/local/sbin/mk-desktop
RUN     echo "[michalklabisz/xrdp:${DOCKER_TAG}] <https://gitlab.com/mk_cafe/docker-xrdp>" > /mk/version \
    &&  echo "[version: ${CI_COMMIT_REF_NAME}@${CI_COMMIT_SHA}]" >> /mk/version \
    &&  /usr/local/sbin/mk-desktop ${DOCKER_TAG}

# Copy source files
COPY Dockerfile LICENSE README.md /mk/

EXPOSE 22
EXPOSE 3389

VOLUME [ "/home" ]
WORKDIR /home

CMD [ "/usr/local/sbin/mk-start" ]